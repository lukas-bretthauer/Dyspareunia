# Dyspareunia

*Dyspareunia (noun) – difficult or painful sexual intercourse.*

This mod introduces a wear & tear system to RimJobWorld.

## Features

- Penetrating sex may damage pawns' "tender parts", causing abrasions and/or ruptures
- Amount of damage depends on the relative size of the organ, participants' traits, wetness and whether the sex is consensual
- Vaginas and anuses stretch from being penetrated by bigger penises
- However, they also gradually contract over time until they reach 50% ("average") size
- Tight penetrations give both partners positive thoughts (only applies to vaginas and anuses and doesn't apply to rape victims)
- Different kinds of organs have different effect, e.g. hydraulic vaginas don't stretch, barbed penises cause more damage etc.
- Childbirth (both RJW and Biotech) also causes damage to vagina and stretches it
- You can make organs indestructible (then they won't be damaged beyond a certain point)

## Requirements & Load Order

- Harmony
- RimWorld (Core)
- RimJobWorld
- Dyspareunia (this mod)

## Notes

- Built-in compatibility with Biotech (genes affect damage), Ideology, RJW-Menstruation (coitus during menstruation damages more) and RJW-Ex (plugs)
- There is a degree of randomness in dealing damage, so no two intercourses are the same
- See plans and known issues here: https://gitgud.io/NuttySquabble/Dyspareunia/issues
