﻿using RimWorld;
using rjw;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Verse;

namespace Dyspareunia
{
    public static class PenetrationUtility
    {
        public static void ApplyOrganProperties(Hediff organ, ref float rubbingDamage, ref float stretchDamage, bool isRape = false)
        {
            if (organ is null)
                return;
            Dyspareunia.Log($"ApplyOrganProperties({organ?.def?.defName}, {rubbingDamage}, {stretchDamage}, {isRape})");
            float oldRubbingDamage = rubbingDamage, oldStretchDamage = stretchDamage;

            switch (organ.def.defName)
            {
                case "PegDick":
                    rubbingDamage *= 1.4f;
                    break;

                case "HydraulicPenis":
                    rubbingDamage *= 1.2f;
                    break;

                case "HydraulicVagina":
                case "HydraulicAnus":
                    rubbingDamage *= 0.8f;
                    stretchDamage *= 0.8f;
                    break;

                case "BionicPenis":
                    if (!isRape)
                        rubbingDamage *= 0.6f;
                    break;

                case "BionicVagina":
                case "BionicAnus":
                    rubbingDamage *= 0.6f;
                    break;

                case "ArchotechPenis":
                    if (!isRape)
                    {
                        rubbingDamage *= 0.5f;
                        stretchDamage *= 0.6f;
                    }
                    break;

                case "ArchotechVagina":
                case "ArchotechAnus":
                    rubbingDamage *= 0.4f;
                    stretchDamage *= 0.4f;
                    break;
            }

            List<string> partProps = organ.def.GetModExtension<PartProps>()?.props;
            if (partProps != null)
                foreach (string prop in partProps)
                {
                    Dyspareunia.Log($"{organ.Label} is {prop}.");
                    switch (prop.ToLower())
                    {
                        case "loose":
                            stretchDamage *= 0.6f;
                            break;

                        case "deep":
                            rubbingDamage *= 0.8f;
                            stretchDamage *= 0.8f;
                            break;

                        case "tight":
                            stretchDamage *= 1.4f;
                            break;

                        case "barbed":
                        case "ridged":
                            rubbingDamage *= 1.5f;
                            break;

                        case "long":
                            rubbingDamage *= 1.2f;
                            break;

                        case "thin":
                            stretchDamage *= 0.6f;
                            break;

                        case "rigid":
                            rubbingDamage *= 1.2f;
                            stretchDamage *= 1.2f;
                            break;

                        case "small":
                            stretchDamage *= 0.6f;
                            break;

                        case "solid":
                            if (organ.IsOrifice())
                                stretchDamage *= 0.6f;
                            break;

                        case "resizable":
                            if (!isRape || organ.IsOrifice())
                                stretchDamage *= 0.5f;
                            break;
                    }
                }

            // RJW-Menstruation: menstruating vaginas take 20% more rubbing damage
            if (organ.IsVagina() && organ.pawn.health.hediffSet.hediffs.Any(hediff => hediff.Part == organ.Part && hediff.def.defName == "Hediff_MenstrualCramp"))
            {
                Dyspareunia.Log($"{organ.def.defName} has menstruation and receives more damage.");
                rubbingDamage *= 1.2f;
            }

            if (oldRubbingDamage != rubbingDamage || oldStretchDamage != stretchDamage)
            {
                Dyspareunia.Log($"Rubbing damage: {oldRubbingDamage:F2} -> {rubbingDamage:F2}");
                Dyspareunia.Log($"Stretch damage: {oldStretchDamage:F2} -> {stretchDamage:F2}");
            }
        }

        public static void ApplyDamage(DamageDef damageDef, float damage, Pawn instigator, Pawn target, BodyPartRecord part)
        {
            if (Mod.Settings.indestructibleParts)
            {
                Dyspareunia.Log($"Preserving {part}: part health: {target.health.hediffSet.GetPartHealth(part):F2}; original damage {damage:F2}...");
                damage = Math.Min(damage, target.health.hediffSet.GetPartHealth(part) - 1);
                Dyspareunia.Log($"Safe damage: {damage:F2}");
            }

            // If damage is too low or negative, skip it
            if (damage < 0.5f)
                return;

            DamageInfo damageInfo = new DamageInfo(damageDef, damage, instigator: instigator, hitPart: part);
            damageInfo.SetAllowDamagePropagation(false);
            damageInfo.SetIgnoreArmor(true);
            Dyspareunia.Log($"Applying damage {damageInfo} to {damageInfo.HitPart?.Label}.");
            target.TakeDamage(damageInfo);
        }

        public static void StretchOrgan(Hediff organ, float amount)
        {
            if (amount <= 0)
                return;
            float stretch = amount / organ.Part.def.hitPoints * Mod.Settings.stretchFactor;
            Dyspareunia.Log($"Stretching {organ.def} (severity {organ.Severity:F2}) by {stretch:F2}");
            organ.Severity += stretch;
        }

        const float MouthRelativeSize = 1 / 0.15f;  // Mouth (jaw) coverage for humanlikes = 0.15. For pawns with bodysize = 1, mouth will be the same size as vagina

        public static void ProcessSexDamage(Pawn penetrator, float penetratingOrganSize, Hediff orifice, bool isRape, Pawn target = null, BodyPartRecord orificeBPR = null, Hediff penetratingOrgan = null, bool applyRubbingDamage = true)
        {
            if (orificeBPR is null)
            {
                orificeBPR = orifice?.Part;
                if (orificeBPR is null)
                {
                    Dyspareunia.Log("Orifice part not found.");
                    return;
                }
            }
            // Checking validity of target
            if (target is null)
            {
                target = orifice?.pawn;
                if (target is null)
                {
                    Dyspareunia.Log("Target pawn not found.");
                    return;
                }
            }
            float orificeSize = orifice is null ? target.BodySize * orificeBPR.coverage * MouthRelativeSize : orifice.GetOrganSize();
            Dyspareunia.Log($"Processing damage from {penetrator} (effective size {penetratingOrganSize:F2}) penetrating {target}'s {orifice?.def?.defName ?? orificeBPR.def?.defName} (effective size {orificeSize:F2}).");

            // Calculating damage amounts
            float relativeSize = penetratingOrganSize / orificeSize;
            float rubbingDamage = applyRubbingDamage ? 0.5f : 0;
            float stretchDamage = Math.Max(relativeSize - 1, 0);

            if (relativeSize > 1.25f)
                rubbingDamage *= 2; // If penetrating organ is much bigger than the orifice, rubbing damage is higher

            if (isRape)
            {
                rubbingDamage *= 2;
                stretchDamage *= 2;
            }

            if (penetrator != null)
            {

                if (penetrator.story?.traits != null)
                {
                    if (penetrator.story.traits.HasTrait(TraitDefOf.Bloodlust))
                    {
                        rubbingDamage *= 1.5f;
                        stretchDamage *= 1.25f;
                    }

                    if (penetrator.story.traits.HasTrait(xxx.rapist))
                    {
                        rubbingDamage *= 1.5f;
                        stretchDamage *= 1.25f;
                    }

                    if (penetrator.story.traits.HasTrait(TraitDefOf.Psychopath))
                    {
                        rubbingDamage *= 1.4f;
                        stretchDamage *= 1.2f;
                    }

                    if ((penetrator.story.traits.HasTrait(TraitDefOf.DislikesMen) && target.gender == Gender.Male)
                        || (penetrator.story.traits.HasTrait(TraitDefOf.DislikesWomen) && target.gender == Gender.Female))
                    {
                        rubbingDamage *= 1.2f;
                        stretchDamage *= 1.1f;
                    }

                    if (penetrator.story.traits.HasTrait(TraitDefOf.Kind))
                    {
                        rubbingDamage *= 0.6f;
                        stretchDamage *= 0.8f;
                    }

                    if (penetrator.story.traits.HasTrait(TraitDefOf.Wimp))
                    {
                        rubbingDamage *= 0.6f;
                        stretchDamage *= 0.8f;
                    }
                }

                if (isRape && penetrator.WorkTagIsDisabled(WorkTags.Violent))
                {
                    rubbingDamage *= 0.6f;
                    stretchDamage *= 0.6f;
                }

                if (ModLister.BiotechInstalled && penetrator.genes != null)
                {
                    if (penetrator.genes.HasGene("KillThirst"))
                    {
                        rubbingDamage *= 1.4f;
                        stretchDamage *= 1.2f;
                    }

                    if (isRape && penetrator.genes.HasGene("Aggression_DeadCalm") && !penetrator.WorkTagIsDisabled(WorkTags.Violent))
                    {
                        rubbingDamage *= 0.7f;
                        stretchDamage *= 0.7f;
                    }

                    if (penetrator.genes.HasGene("Aggression_Aggressive"))
                    {
                        rubbingDamage *= 1.2f;
                        stretchDamage *= 1.1f;
                    }

                    if (penetrator.genes.HasGene("Aggression_HyperAggressive"))
                    {
                        rubbingDamage *= 1.5f;
                        stretchDamage *= 1.25f;
                    }

                    if (penetrator.genes.HasGene("MeleeDamage_Weak"))
                    {
                        rubbingDamage *= 0.8f;
                        stretchDamage *= 0.9f;
                    }

                    if (isRape && penetrator.genes.HasGene("MeleeDamage_Strong"))
                    {
                        rubbingDamage *= 1.2f;
                        stretchDamage *= 1.1f;
                    }
                }

                if (penetrator.Ideo != null && penetrator.Ideo.IsPreferredXenotype(target))
                {
                    rubbingDamage *= 0.7f;
                    stretchDamage *= 0.7f;
                }
            }

            if (target.story?.traits != null)
            {
                if (target.story.traits.HasTrait(TraitDefOf.Wimp))
                {
                    rubbingDamage *= 1.2f;
                    stretchDamage *= 1.2f;
                }

                if (target.story.traits.HasTrait(TraitDefOf.Masochist))
                {
                    rubbingDamage *= 1.2f;
                    stretchDamage *= 1.1f;
                }

                if (target.story.traits.HasTrait("Nimble"))
                {
                    rubbingDamage *= 0.8f;
                    stretchDamage *= 0.9f;
                }
            }

            ApplyOrganProperties(penetratingOrgan, ref rubbingDamage, ref stretchDamage, isRape);
            ApplyOrganProperties(orifice, ref rubbingDamage, ref stretchDamage, isRape);

            Dyspareunia.Log($"Rubbing damage before randomization: {rubbingDamage:F2}");
            Dyspareunia.Log($"Stretch damage before randomization: {stretchDamage:F2}");

            // Applying randomness
            rubbingDamage *= RandomGaussianFactorClamped(0.25f);
            stretchDamage *= RandomGaussianFactorClamped(0.25f);

            Dyspareunia.Log($"Rubbing damage before lubricant: {rubbingDamage:F2}");
            Dyspareunia.Log($"Stretch damage before lubricant: {stretchDamage:F2}");

            // Stretching the orifice
            if (orifice != null)
                StretchOrgan(orifice, stretchDamage);

            // Applying lubrication
            float wetness = target.GetWetness(orificeBPR, orifice);
            Dyspareunia.Log($"Total wetness: {wetness}");
            rubbingDamage *= Math.Max(1 - wetness * 0.5f, 0.25f);
            stretchDamage *= Math.Max(1 - wetness * 0.5f, 0.4f);

            Dyspareunia.Log($"Rubbing damage final: {rubbingDamage:F2}");
            Dyspareunia.Log($"Stretch damage final: {stretchDamage:F2}");
            Dyspareunia.Log($"Damage factor: {Mod.Settings.damageFactor:P0}");

            // Adding a single hediff based on which damage type is stronger (to reduce clutter in the Health view and save on the number of treatments)
            ApplyDamage(rubbingDamage > stretchDamage ? DamageDefOf.SexRub : DamageDefOf.SexStretch, (rubbingDamage + stretchDamage) * Mod.Settings.damageFactor, isRape ? penetrator : null, target, orificeBPR);

            // Applying positive moodlets for a big dick
            if (relativeSize > 1.25
                || (penetratingOrgan?.def?.defName == "ArchotechPenis" && relativeSize >= 1)
                || (!isRape && (orifice?.def?.defName == "ArchotechVagina" || orifice?.def?.defName == "ArchotechAnus") && relativeSize >= 1))
            {
                if (penetrator?.needs?.mood?.thoughts?.memories != null)
                    penetrator.needs.mood.thoughts.memories.TryGainMemory(ThoughtMaker.MakeThought(ThoughtDef.Named("TightLovin"), relativeSize < 2 ? 0 : 1));
                if (!isRape && target.needs?.mood?.thoughts?.memories != null && orifice?.def?.defName != "HydraulicVagina" && orifice?.def?.defName != "HydraulicAnus")
                    target.needs.mood.thoughts.memories.TryGainMemory(ThoughtMaker.MakeThought(ThoughtDef.Named("BigDick"), relativeSize < 2 ? 0 : 1));
            }
        }

        public static void ProcessSexDamage(Hediff penetratingOrgan, Hediff orifice, bool isRape)
        {
            if (penetratingOrgan is null)
                Dyspareunia.Log("No penetrating organ found.");
            else if (orifice is null)
                Dyspareunia.Log("No orifice found.");
            else ProcessSexDamage(penetratingOrgan.pawn, penetratingOrgan.GetOrganSize(), orifice, isRape, penetratingOrgan: penetratingOrgan);
        }

        /// <summary>
        /// This method discovers all penetrations in the sex act and applies respective damage
        /// </summary>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        /// <param name="rape"></param>
        /// <param name="sextype"></param>
        /// <returns>Empty list if no eligible penetrations found, or an element for each penetration (can be DP etc.)</returns>
        public static void ProcessSex(SexProps props)
        {
            Pawn p1 = props.pawn, p2 = props.partner;
            bool reverse = false;
            Dyspareunia.Log($"Checking {props.sexType} {(props.isRape ? "rape" : "sex")} between {p1} (body size {p1.BodySize}) and {p2} (body size {p2.BodySize}).");

            switch (props.sexType)
            {
                case xxx.rjwSextype.Vaginal:
                    Hediff penis = p1.GetPenisOrOvipositor();
                    Hediff orifice = p2.GetVagina();
                    if (penis is null || orifice is null)
                    {
                        penis = p2.GetPenisOrOvipositor();
                        orifice = p1.GetVagina();
                        reverse = true;
                    }
                    ProcessSexDamage(penis, orifice, props.isRape && !reverse);
                    break;

                case xxx.rjwSextype.Anal:
                    penis = p1.GetPenisOrOvipositor();
                    orifice = p2.GetAnus();
                    if (penis is null || orifice is null)
                    {
                        penis = p2.GetPenisOrOvipositor();
                        orifice = p1.GetAnus();
                        reverse = true;
                    }
                    ProcessSexDamage(penis, orifice, props.isRape && !reverse);
                    break;

                case xxx.rjwSextype.Oral:
                case xxx.rjwSextype.Fellatio:
                case xxx.rjwSextype.Sixtynine:
                    if (props.isRape)
                    {
                        penis = p1.GetPenisOrOvipositor();
                        BodyPartRecord mouth = p2.GetMouth();
                        if (penis != null && mouth != null)
                            ProcessSexDamage(p1, penis.GetOrganSize(), null, true, p2, mouth, penis);
                    }
                    break;

                case xxx.rjwSextype.DoublePenetration:
                    Hediff[] penises = p1.GetHemipenisOr2Penises();
                    Hediff[] orifices = p2.GetAny2Orifices();
                    if (penises.NullOrEmpty() || orifices.Length < 2)
                    {
                        penises = p2.GetHemipenisOr2Penises();
                        orifices = p1.GetAny2Orifices();
                        reverse = true;
                    }
                    if (!penises.NullOrEmpty() && orifices.Length >= 2)
                    {
                        ProcessSexDamage(penises[0], orifices[0], props.isRape && !reverse);
                        ProcessSexDamage(penises[penises.Length - 1], orifices[1], props.isRape && !reverse);
                    }
                    else Dyspareunia.Log($"DoublePenetration between {p1} and {p2}{(reverse ? " (reversed)" : "")} failed: found {(penises is null ? 0 : penises.Length)} penises and {orifices.Length} orifices.");
                    break;

                case xxx.rjwSextype.Fingering:
                    orifice = p2.GetVagina() ?? p2.GetAnus();
                    if (orifice is null)
                    {
                        orifice = p1.GetVagina() ?? p1.GetAnus();
                        reverse = true;
                    }
                    ProcessSexDamage(reverse ? p2 : p1, (reverse ? p2 : p1).GetFingerSize(), orifice, props.isRape && !reverse);
                    break;

                case xxx.rjwSextype.Fisting:
                    orifice = p2.GetVagina() ?? p2.GetAnus();
                    if (orifice is null)
                    {
                        orifice = p1.GetVagina() ?? p1.GetAnus();
                        reverse = true;
                    }
                    ProcessSexDamage(reverse ? p2 : p1, (reverse ? p2 : p1).GetHandSize(), orifice, props.isRape && !reverse);
                    break;

                case xxx.rjwSextype.MechImplant:
                    Dyspareunia.Log($"Processing mech implant sex between {p1} and {p2}");
                    orifice = p2.GetVagina() ?? p2.GetAnus();
                    if (orifice is null && p2.kindDef.race.race.FleshType == FleshTypeDefOf.Mechanoid)
                    {
                        orifice = p1.GetVagina() ?? p1.GetAnus();
                        reverse = true;
                    }
                    ProcessSexDamage(reverse ? p2 : p1, (reverse ? p2 : p1).BodySize, orifice, props.isRape && !reverse);
                    break;
            }
        }

        public static float RandomGaussianFactorClamped(float widthFactor = 1, float min = 0.5f, float max = 2) => Mathf.Clamp(RandomGaussianFactor(widthFactor), min, max);

        public static float RandomGaussianFactor(float widthFactor = 1) => Mathf.Pow(2, Rand.Gaussian(0, widthFactor));
    }
}
