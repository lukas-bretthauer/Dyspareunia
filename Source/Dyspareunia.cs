﻿using HarmonyLib;
using rjw;
using System;
using System.Collections.Generic;
using Verse;
using System.Reflection;
using System.Linq;
using RimWorld;

namespace Dyspareunia
{
    [StaticConstructorOnStartup]
    public static class Dyspareunia
    {
        static Harmony harmony;

        static Dyspareunia()
        {
            Log("Initializing...");

            harmony = new Harmony("nuttysquabble.dyspareunia");

            // Patches for RJW
            harmony.Patch(typeof(SexUtility).GetMethod("Aftersex"), prefix: new HarmonyMethod(typeof(Dyspareunia).GetMethod("SexUtility_Prefix")));
            harmony.Patch(typeof(Hediff_PartBaseNatural).GetMethod("Tick"), postfix: new HarmonyMethod(typeof(Dyspareunia).GetMethod("PartBase_Tick_Postfix")));
            harmony.Patch(typeof(Hediff_PartBaseArtifical).GetMethod("Tick"), postfix: new HarmonyMethod(typeof(Dyspareunia).GetMethod("PartBase_Tick_Postfix")));
            harmony.Patch(typeof(Hediff_BasePregnancy).GetMethod("PostBirth"), postfix: new HarmonyMethod(typeof(Dyspareunia).GetMethod("Hediff_BasePregnancy_Patch")));
            harmony.Patch(typeof(PregnancyUtility).GetMethod("ApplyBirthOutcome"), postfix: new HarmonyMethod(typeof(Dyspareunia).GetMethod("PregnancyUtility_ApplyBirthOutcome_Postfix")));

            // Patches for RJW-Ex
            MethodInfo methodInfo = AccessTools.Method("rjwex.anal_plug_soul:on_wear");
            if (methodInfo != null)
            {
                Log("Patching RJW-ex...");
                harmony.Patch(methodInfo, postfix: new HarmonyMethod(typeof(RJWEx_Patches).GetMethod("anal_plug_soul_on_wear_Patch")));
                harmony.Patch(AccessTools.Method("rjwex.JobDriver_UseFM:stopSession"), postfix: new HarmonyMethod(typeof(RJWEx_Patches).GetMethod("JobDriver_UseFM_stopSession")));
            }

            CumHediffDef = DefDatabase<HediffDef>.GetNamedSilentFail("Hediff_Cum");
        }

        internal static void Log(string message, bool important = false)
        {
            if (important || Mod.Settings.debugLogging)
                Verse.Log.Message($"[Dyspareunia] {DateTime.UtcNow}: {message}");
        }

        public static HediffDef CumHediffDef;

        public static float GetPlugSize(int rjwSize, Hediff anus) => rjwSize == 0 ? Math.Min(anus.Severity * 3, 3) : (0.5f + rjwSize / 2);

        /// <summary>
        /// Harmony postfix method for SexUtility.ProcessSex. It calculates and applies damage and other effects
        /// </summary>
        /// <param name="pawn">Pawn 1 (rapist, whore etc.)</param>
        /// <param name="partner">Pawn 2 (victim, client etc.)</param>
        /// <param name="rape">True if it's a non-consensual sex</param>
        /// <param name="sextype">Sex type (only Vaginal, Anal and Double Penetration are supported ATM)</param>
        public static void SexUtility_Prefix(SexProps props) => PenetrationUtility.ProcessSex(props);

        /// <summary>
        /// Harmony postfix method for Hediff_PartBaseNatural.Tick and Hediff_PartBaseNatural.Tick. Applies organ contraction
        /// </summary>
        /// <param name="__instance"></param>
        public static void PartBase_Tick_Postfix(HediffWithComps __instance)
        {
            // Only runs once per 14 hours 40 minutes (to contract by 50% in 30 days)
            if (!__instance.pawn.IsHashIntervalTick(36000))
                return;

            // If Contraction Time setting is set to 0, contraction is disabled
            if (Mod.Settings.contractionTime <= 0)
                return;

            // Skip unspawned pawns
            if (!__instance.pawn.Spawned)
                return;

            // Only contract organs more than 0.5 in severity
            if (__instance.Severity <= Mod.Settings.contractionTarget)
                return;

            // Only works for orifices
            if (!__instance.IsOrifice())
                return;

            // HydraulicVagina and HydraulicAnus don't contract
            if (__instance.def.defName == "HydraulicVagina" || __instance.def.defName == "HydraulicAnus")
                return;

            // If current orifice is an anus, checking if there is an anal plug from rjw-ex mod
            if (__instance.IsAnus() && __instance.pawn?.apparel != null && __instance.pawn.apparel.WornApparel.Any(a => a.def.thingClass.FullName == "rjwex.anal_plug"))
            {
                Log($"A plug is found in {__instance.pawn}'s {__instance}. No contraction.");
                return;
            }

            // Contract the part by 1%
            __instance.Heal(0.3f / Mod.Settings.contractionTime);
        }

        static int lastBirthTick;
        static List<Pawn> gaveBirthThisTick = new List<Pawn>();

        const float BirthDamageMultiplier = 3;

        static void ApplyBirthDamage(Pawn mother, Thing baby)
        {
            Log($"ApplyBirthDamage for {mother} and {baby}");

            if (mother?.health?.hediffSet is null)
            {
                Log("No hediffSet found for the mother!", true);
                if (mother != null)
                    Log($"Mother: {mother}", true);
                return;
            }

            Hediff vagina = mother.GetVagina();
            if (vagina?.Part is null)
            {
                Log($"No vagina found for {mother}!", true);
                return;
            }

            Log($"Vagina original size: {vagina.Severity}; effective size: {vagina.GetOrganSize()}; HP: {vagina.Part.def.hitPoints}");

            float babySize;
            if (baby is Corpse)
                baby = ((Corpse)baby).InnerPawn;
            if (baby is Pawn)
                babySize = ((Pawn)baby).BodySize;
            else
            {
                Log($"Baby not found for {mother}! Guessing its size.", true);
                LifeStageAge babyLifeStage = mother.RaceProps.lifeStageAges.Find(lifeStage => lifeStage.minAge == 0);
                if (babyLifeStage is null)
                    babySize = 0.2f;
                else babySize = babyLifeStage.def.bodySizeFactor;
                babySize *= mother.BodySize;
            }
            Log($"Baby size: {babySize}");

            float damage = babySize / vagina.GetOrganSize() * vagina.Part.def.hitPoints * Math.Min(mother.HealthScale, mother.BodySize) * BirthDamageMultiplier * PenetrationUtility.RandomGaussianFactorClamped(0.25f) * Mod.Settings.damageFactor;
            Log($"Childbirth damage: {damage} HP");
            if (damage > 0)
            {
                PenetrationUtility.StretchOrgan(vagina, damage);
                damage *= Math.Max(1 - mother.GetWetness(vagina.Part, vagina) * 0.5f, 0.4f);
                PenetrationUtility.ApplyDamage(DamageDefOf.SexStretch, damage, null, mother, vagina.Part);
            }
        }

        /// <summary>
        /// Harmony patch for childbirth damage
        /// </summary>
        /// <param name="__instance"></param>
        /// <param name="baby"></param>
        public static void Hediff_BasePregnancy_Patch(Pawn mother, Pawn baby)
        {
            if (mother is null)
                return;
            // Checking if this mother has already given birth in current tick (damage applies only once)
            if (lastBirthTick != (lastBirthTick = Find.TickManager.TicksGame))
                gaveBirthThisTick.Clear();
            else if (gaveBirthThisTick.Contains(mother))
            {
                Log($"{mother} has already given birth this tick. No more damage is applied.");
                return;
            }

            // Remember this mother, so as not to apply damage again if she has several babies
            gaveBirthThisTick.Add(mother);
            ApplyBirthDamage(mother, baby);
        }

        public static void PregnancyUtility_ApplyBirthOutcome_Postfix(Thing __result, Thing birtherThing)
        {
            if (!(birtherThing is Pawn))
                return;
            ApplyBirthDamage(birtherThing as Pawn, __result);
        }
    }
}
