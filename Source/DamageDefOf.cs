﻿using RimWorld;
using Verse;

namespace Dyspareunia
{
    [DefOf]
    public class DamageDefOf
    {
        public static DamageDef SexRub;
        public static DamageDef SexStretch;
    }
}
