﻿using HarmonyLib;
using RimWorld;
using System;
using Verse;
using Verse.AI;

namespace Dyspareunia
{
    static class RJWEx_Patches
    {
        public static void anal_plug_soul_on_wear_Patch(Pawn wearer, Apparel gear)
        {
            Dyspareunia.Log($"RJWEx_Patches.anal_plug_soul_on_wear_Patch(..., '{wearer}', '{gear.def.defName}')");
            try
            {
                int size = (int)AccessTools.Field(gear.def.GetType(), "plug_size").GetValue(gear.def);
                Dyspareunia.Log($"The plug is size {size}");
                Hediff anus = wearer.GetAnus();
                PenetrationUtility.ProcessSexDamage(null, Dyspareunia.GetPlugSize(size, anus), anus, false, applyRubbingDamage: false);
            }
            catch (Exception e)
            {
                Dyspareunia.Log(e.Message, true);
                return;
            }
        }

        public static void JobDriver_UseFM_stopSession(JobDriver __instance)
        {
            Dyspareunia.Log($"RJWEx_Patches.JobDriver_UseFM_stopSession for {__instance.pawn}");
            Hediff orifice = __instance.pawn.GetAnyOrifice();
            if (orifice != null)
                PenetrationUtility.ProcessSexDamage(null, 1.5f, orifice, false);
        }
    }
}
