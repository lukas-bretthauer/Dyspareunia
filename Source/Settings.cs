﻿using Verse;

namespace Dyspareunia
{
    public class Settings : ModSettings
    {
        public float damageFactor = 1;
        public bool indestructibleParts;
        public float stretchFactor = 1;
        public float contractionTime = 30;
        public float contractionTarget = 0.5f;
        public bool debugLogging = false;

        public override void ExposeData()
        {
            Scribe_Values.Look(ref damageFactor, "damageFactor", 1);
            Scribe_Values.Look(ref indestructibleParts, "indestructibleParts");
            Scribe_Values.Look(ref stretchFactor, "stretchFactor", 1);
            Scribe_Values.Look(ref contractionTime, "contractionTime", 30);
            Scribe_Values.Look(ref contractionTarget, "contractionTarget", 0.5f);
            Scribe_Values.Look(ref debugLogging, "debugLogging");
        }
    }
}
