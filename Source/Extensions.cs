﻿using RimWorld;
using rjw;
using rjw.Modules.Shared.Extensions;
using System.Collections.Generic;
using System.Linq;
using Verse;

namespace Dyspareunia
{
    public static class Extensions
    {
        public static bool IsPrivatePart(this Hediff hediff) => hediff is Hediff_PartBaseNatural || hediff is Hediff_PartBaseArtifical;

        public static bool IsVagina(this Hediff hediff) => hediff.def.defName.ToLower().Contains("vagina");

        public static bool IsAnus(this Hediff hediff) => hediff.Part.def.defName.ToLower().Contains("anus");

        public static bool IsOrifice(this Hediff hediff) => hediff.IsVagina() || hediff.IsAnus();

        public static bool IsMouth(this BodyPartRecord part) => part.def.tags.Contains(RimWorld.BodyPartTagDefOf.EatingSource) && !part.def.defName.Contains("beak");

        public static bool IsPenisOrOvipositor(this Hediff hediff) => Genital_Helper.is_penis(hediff) || hediff.def.defName.ToLower().Contains("ovipositorf");

        public static bool IsHemipenis(this Hediff hediff) => hediff.def.defName.ToLower().Contains("hemipenis");

        public static IEnumerable<Hediff> GetPrivateParts(this Pawn pawn) => pawn.health.hediffSet.hediffs.Where(hediff => hediff.IsPrivatePart());

        public static Hediff GetVagina(this Pawn pawn) => pawn.GetPrivateParts().FirstOrDefault(hed => hed.IsVagina());

        public static Hediff GetAnus(this Pawn pawn) => pawn.GetPrivateParts().FirstOrDefault(hed => hed.IsAnus());

        public static Hediff GetAnyOrifice(this Pawn pawn) => pawn.GetPrivateParts().FirstOrDefault(hediff => hediff.IsVagina() || hediff.IsAnus());

        public static Hediff[] GetAny2Orifices(this Pawn pawn) => pawn.GetPrivateParts().Where(hediff => hediff.IsVagina() || hediff.IsAnus()).Take(2).ToArray();

        public static BodyPartRecord GetMouth(this Pawn pawn) => pawn?.RaceProps?.body?.AllParts.Find(part => part.IsMouth() && !part.IsMissingForPawn(pawn));

        public static Hediff GetPenisOrOvipositor(this Pawn pawn) =>
          Genital_Helper.penis_blocked(pawn) ? null : pawn.GetPrivateParts().FirstOrDefault(hed => hed.IsPenisOrOvipositor());

        public static Hediff GetHemipenis(this Pawn pawn) => pawn.GetPrivateParts().FirstOrDefault(hediff => hediff.IsHemipenis());

        public static Hediff[] GetHemipenisOr2Penises(this Pawn pawn)
        {
            Hediff[] penises = new Hediff[] { pawn.GetHemipenis() };
            if (penises[0] != null)
                return penises;
            penises = pawn.GetPrivateParts().Where(hediff => hediff.IsPenisOrOvipositor()).Take(2).ToArray();
            if (penises.Length < 2)
                return null;
            return penises;
        }

        /// <summary>
        /// Returns the size of an organHediff; 0.5 to 1.5 for (normal) anus; 1 to 2 for other organs
        /// </summary>
        public static float GetOrganSize(this Hediff organ) =>
            ((organ.IsAnus() ? 0.5f : 1) + organ.Severity) * organ.pawn.BodySize;  // Assume max natural organHediff size is 2x bigger than the smallest

        /// <summary>
        /// Returns the size (calculated as coverage * body size * 10) of the pawn's biggest finger
        /// </summary>
        /// <param name="pawn"></param>
        /// <returns></returns>
        public static float GetFingerSize(this Pawn pawn)
        {
            if (pawn?.RaceProps?.body is null)
            {
                Dyspareunia.Log($"The pawn {pawn?.Label ?? "NULL"} has no body!", true);
                return 0;
            }

            BodyPartRecord biggestFinger = pawn.RaceProps.body.AllParts
                .FindAll(bpr => bpr.def.tags.Contains(RimWorld.BodyPartTagDefOf.ManipulationLimbDigit) && !pawn.health.hediffSet.PartIsMissing(bpr))
                .MaxByWithFallback(bpr => bpr.coverage);
            if (biggestFinger is null)
            {
                Dyspareunia.Log($"No fingers found for {pawn}!", true);
                return 0;
            }
            Dyspareunia.Log($"Biggest finger: {biggestFinger.Label}; coverage: {biggestFinger.coverage:P1}");
            return biggestFinger.coverage * pawn.BodySize * 10;
        }

        /// <summary>
        /// Returns the size (calculated as coverage * body size * 10) of the pawn's hand
        /// </summary>
        /// <param name="pawn"></param>
        /// <returns></returns>
        public static float GetHandSize(this Pawn pawn)
        {
            if (pawn?.RaceProps?.body is null)
            {
                Dyspareunia.Log($"The pawn {pawn?.Label ?? "NULL"} has no body!", true);
                return 0;
            }
            if (pawn.health?.hediffSet is null)
            {
                Dyspareunia.Log($"The pawn {pawn} has no hediffSet.", true);
                return 0;
            }

            BodyPartRecord hand = pawn.RaceProps.body.GetPartsWithDef(BodyPartDefOf.Hand)?.FirstOrFallback();
            if (hand is null)
            {
                Dyspareunia.Log($"{pawn} has no hands!", true);
                return 0;
            }
            Dyspareunia.Log($"{pawn}'s hand: {hand.Label}; coverage: {hand.coverage:P1}");
            float size = hand.coverage * pawn.BodySize * 10;
            Dyspareunia.Log($"Hand size: {size}");
            return size;
        }

        public static float GetWetness(this Pawn pawn, BodyPartRecord organBPR, Hediff organHediff)
        {
            float amount = 0;

            // Getting wetness
            if (organHediff != null)
            {
                CompHediffBodyPart hediffComp = organHediff.TryGetComp<CompHediffBodyPart>();
                if (hediffComp != null && hediffComp.FluidAmmount > 0)
                    amount = hediffComp.FluidAmmount * hediffComp.FluidModifier / pawn.BodySize;
            }

            // Adding semen to the amount of fluids if RJW Cum is present
            if (Dyspareunia.CumHediffDef != null)
                amount += pawn.health.hediffSet.hediffs.Where(hediff => hediff.def == Dyspareunia.CumHediffDef && hediff.Part == organBPR).Sum(hediff => hediff.Severity);

            if (amount > 0)
                Dyspareunia.Log($"Found {amount:F2} fluid in {organHediff?.Label ?? organBPR?.Label}.");
            return amount / pawn.BodySize * 0.1f;
        }

        public static bool HasTrait(this TraitSet traits, string traitDefName) => traits != null && traits.allTraits.Any(trait => trait.def.defName == traitDefName);

        public static bool HasGene(this Pawn_GeneTracker genes, string geneDefName) => genes != null && genes.GenesListForReading.Any(gene => gene.def.defName == geneDefName);
    }
}
